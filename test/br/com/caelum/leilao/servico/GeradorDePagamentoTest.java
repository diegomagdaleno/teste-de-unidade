package br.com.caelum.leilao.servico;

import static org.hamcrest.Matchers.equalTo;

import static org.junit.Assert.assertThat;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import br.com.caelum.leilao.builder.CriadorDeLeilao;
import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.dominio.Pagamento;
import br.com.caelum.leilao.dominio.Usuario;
import br.com.caelum.leilao.infra.dao.RepositorioDeLeiloes;
import br.com.caelum.leilao.infra.dao.RepositorioDePagamentos;
import br.com.caelum.leilao.util.Calendario;
import br.com.caelum.leilao.util.CalendarioImpl;

public class GeradorDePagamentoTest {
	
	private RepositorioDeLeiloes leiloes;
	private RepositorioDePagamentos pagamentos;
	private Avaliador avaliador;
	private Calendario calendario;

	@Before
	public void setUp(){
		leiloes = mock(RepositorioDeLeiloes.class);
		pagamentos = mock(RepositorioDePagamentos.class);
		calendario = mock(Calendario.class);
		avaliador = new Avaliador();
	}

    @Test
    public void deveGerarPagamentoParaUmLeilaoEncerrado() {

    	Leilao leilao = new CriadorDeLeilao()
            .para("Playstation")
            .lance(new Usuario("José da Silva"), 2000.0)
            .lance(new Usuario("Maria Pereira"), 2500.0)
            .constroi();

        when(leiloes.encerrados()).thenReturn(Arrays.asList(leilao));

        GeradorDePagamento gerador = new GeradorDePagamento(leiloes, pagamentos, avaliador, new CalendarioImpl());
        gerador.gera();
        
        // criamos o ArgumentCaptor que sabe capturar um Pagamento
        ArgumentCaptor<Pagamento> argumento = ArgumentCaptor.forClass(Pagamento.class);
        // capturamos o Pagamento que foi passado para o método salvar
        verify(pagamentos).salvar(argumento.capture());
        
        assertThat(argumento.getValue().getValor(), equalTo(2500.0));
    }
    
    @Test
    public void deveEmpurrarParaOProximoDiaUtilSeForSabado() {
	
	    Leilao leilao = new CriadorDeLeilao()
		    .para("Playstation")
		    .lance(new Usuario("José da Silva"), 2000.0)
		    .lance(new Usuario("Maria Pereira"), 2500.0)
		    .constroi();
	    
	    // dia 7 de abril de 2012 foi um sabado
	    Calendar sabado = Calendar.getInstance();
	    sabado.set(2012, Calendar.APRIL, 7);
	
	    when(leiloes.encerrados()).thenReturn(Arrays.asList(leilao));
	    when(calendario.hoje()).thenReturn(sabado);
	
	    GeradorDePagamento gerador = new GeradorDePagamento(leiloes, pagamentos, avaliador, calendario);
	    gerador.gera();
	
	    ArgumentCaptor<Pagamento> argumento = ArgumentCaptor.forClass(Pagamento.class);
	    verify(pagamentos).salvar(argumento.capture());
	    Pagamento pagamentoGerado = argumento.getValue();
	
	    assertThat(pagamentoGerado.getData().get(Calendar.DAY_OF_WEEK), equalTo(Calendar.MONDAY));
    }
    
    @Test
    public void deveEmpurrarParaOProximoDiaUtilSeForDomingo() {
    	
    	Leilao leilao = new CriadorDeLeilao()
    			.para("Playstation")
    			.lance(new Usuario("José da Silva"), 2000.0)
    			.lance(new Usuario("Maria Pereira"), 2500.0)
    			.constroi();
    	
    	// dia 8 de abril de 2012 foi um domingo
    	Calendar domingo = Calendar.getInstance();
    	domingo.set(2012, Calendar.APRIL, 8);
    	
    	when(leiloes.encerrados()).thenReturn(Arrays.asList(leilao));
    	when(calendario.hoje()).thenReturn(domingo);
    	
    	GeradorDePagamento gerador = new GeradorDePagamento(leiloes, pagamentos, avaliador, calendario);
    	gerador.gera();
    	
    	ArgumentCaptor<Pagamento> argumento = ArgumentCaptor.forClass(Pagamento.class);
    	verify(pagamentos).salvar(argumento.capture());
    	Pagamento pagamentoGerado = argumento.getValue();
    	
    	assertThat(pagamentoGerado.getData().get(Calendar.DAY_OF_WEEK), equalTo(Calendar.MONDAY));
    }
}