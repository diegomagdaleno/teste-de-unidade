package br.com.caelum.leilao.matcher;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Leilao;

public class LeilaoMatcher extends TypeSafeMatcher<Leilao> {
    private Lance lance;

    public LeilaoMatcher(Lance lance) {
        this.lance = lance;
    }

    @Override
    public boolean matchesSafely(Leilao leilao) {
        return leilao.getLances().contains(this.lance);
    }
    
    @Override
    public void describeTo(Description description) {
        description.appendText("se o leilao tem o lance ").appendText(this.lance.toString());
    }
    
    @Factory
    public static Matcher<Leilao> temUmLance(Lance lance) {
        return new LeilaoMatcher(lance);
    }
}
