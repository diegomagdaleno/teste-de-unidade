package br.com.caelum.leilao.variados;

import org.junit.Assert;
import org.junit.Test;

public class AnoBissextoTest {
	
	@Test
	public void deveVerificarSe4EAnoBissexto() {
		int ano = 4;
		AnoBissexto anoBissexto = new AnoBissexto();
		Assert.assertTrue(anoBissexto.isAnoBissexto(ano));
	}
	
	@Test
	public void deveVerificarSe8EAnoBissexto() {
		int ano = 8;
		AnoBissexto anoBissexto = new AnoBissexto();
		Assert.assertTrue(anoBissexto.isAnoBissexto(ano));
	}
	
	@Test
	public void deveVerificarSe100NaoEAnoBissexto() {
		int ano = 100;
		AnoBissexto anoBissexto = new AnoBissexto();
		Assert.assertFalse(anoBissexto.isAnoBissexto(ano));
	}
	
	@Test
	public void deveVerificarSe200NaoEAnoBissexto() {
		int ano = 200;
		AnoBissexto anoBissexto = new AnoBissexto();
		Assert.assertFalse(anoBissexto.isAnoBissexto(ano));
	}
	
	@Test
	public void deveVerificarSe400EAnoBissexto() {
		int ano = 400;
		AnoBissexto anoBissexto = new AnoBissexto();
		Assert.assertTrue(anoBissexto.isAnoBissexto(ano));
	}
	
	@Test
	public void deveVerificarSe1200EAnoBissexto() {
		int ano = 1200;
		AnoBissexto anoBissexto = new AnoBissexto();
		Assert.assertTrue(anoBissexto.isAnoBissexto(ano));
	}
	
	@Test
	public void deveVerificarSe3NaoEAnoBissexto() {
		int ano = 3;
		AnoBissexto anoBissexto = new AnoBissexto();
		Assert.assertFalse(anoBissexto.isAnoBissexto(ano));
	}
	
	@Test
	public void deveRetornarAnoBissexto() {        
	    AnoBissexto anoBissexto = new AnoBissexto();

	    Assert.assertTrue(anoBissexto.isAnoBissexto(2016));
	    Assert.assertTrue(anoBissexto.isAnoBissexto(2012));
	}

	@Test
	public void naoDeveRetornarAnoBissexto() {        
	    AnoBissexto anoBissexto = new AnoBissexto();        

	    Assert.assertFalse(anoBissexto.isAnoBissexto(2015));
	    Assert.assertFalse(anoBissexto.isAnoBissexto(2011));
	}
}
