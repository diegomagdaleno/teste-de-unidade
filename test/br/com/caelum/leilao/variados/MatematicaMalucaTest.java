package br.com.caelum.leilao.variados;

import org.junit.Assert;
import org.junit.Test;

public class MatematicaMalucaTest {
	@Test
	public void deveVerificarCalculoMalucoMaiorQue30(){
		MatematicaMaluca mat = new MatematicaMaluca();
		int numero = 31;
		Assert.assertEquals(numero*4, mat.contaMaluca(numero));
	}
	
	@Test
	public void deveVerificarCalculoMalucoMaiorQue10MenorQue30(){
		MatematicaMaluca mat = new MatematicaMaluca();
		int numero = 11;
		Assert.assertEquals(numero*3, mat.contaMaluca(numero));
	}
	
	@Test
	public void deveVerificarCalculoMalucoMenorQue10(){
		MatematicaMaluca mat = new MatematicaMaluca();
		int numero = 9;
		Assert.assertEquals(numero*2, mat.contaMaluca(numero));
	}
	
	@Test
	public void deveVerificarCalculoMalucoIgualA10(){
		MatematicaMaluca mat = new MatematicaMaluca();
		int numero = 10;
		Assert.assertEquals(numero*2, mat.contaMaluca(numero));
	}
	
	@Test
	public void deveVerificarCalculoMalucoIgualA30(){
		MatematicaMaluca mat = new MatematicaMaluca();
		int numero = 30;
		Assert.assertEquals(numero*3, mat.contaMaluca(numero));
	}
	
}
