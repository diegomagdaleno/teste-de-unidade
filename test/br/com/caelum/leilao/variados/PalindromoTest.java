package br.com.caelum.leilao.variados;

import org.junit.Assert;
import org.junit.Test;

public class PalindromoTest {
	
	@Test
	public void deveVerificarPalindromoCaseInsensitive(){
		String frase = "os SAraRas SO";
		
		Palindromo p = new Palindromo();
		
		Assert.assertTrue(p.isPalindromo(frase));
	}
	
	@Test
	public void deveVerificarPalindromoCaracteresEspeciais(){
		String frase = "Socorram-me subi no onibus em Marrocos";
		
		Palindromo p = new Palindromo();
		
		Assert.assertTrue(p.isPalindromo(frase));
	}
	
	@Test
	public void deveVerificarPalindromoFalso(){
		String frase = "Não sou um palindromo";
		
		Palindromo p = new Palindromo();
		
		Assert.assertFalse(p.isPalindromo(frase));
	}

}
