package br.com.caelum.leilao.exception;

public class LeilaoSemLanceException extends Exception {

	/**
	 * Generated Serial Version
	 */
	private static final long serialVersionUID = -1855193432208049217L;

	public LeilaoSemLanceException(String mensagem) {
		super(mensagem);
	}

}
