package br.com.caelum.leilao.variados;

public class AnoBissexto {

	public boolean isAnoBissexto(int ano) {
		if(ano%400 == 0)
			return true;
		if(ano%100 == 0)
			return false;
		
		return ano%4 == 0;
	}

}
